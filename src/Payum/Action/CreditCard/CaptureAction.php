<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard;

use Sylius\Bundle\PayumBundle\Model\GatewayConfigInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Omni\Sylius\SwedbankSpp\Client\SwedbankSppClient;
use Omni\Sylius\SwedbankSpp\Communication\SetupTransactionRequest;
use Omni\Sylius\SwedbankSpp\Communication\Transaction;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\Action;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\Amount;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\BillingDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\CustomerDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\HpsTxn;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\MerchantConfiguration;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\PersonalDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\RiskDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\ShippingDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\ThreeDSecure;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\TxnDetails;
use Omni\Sylius\SwedbankSpp\Constants\SwedbankConstants;
use Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard\Api\ApiAwareTrait;
use Omni\Sylius\SwedbankSpp\Response\SetupResponse;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;

class CaptureAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use GatewayAwareTrait, ApiAwareTrait;

    /**
     * @var SwedbankSppClient
     */
    private $sppClient;

    /**
     * @var SwedbankPaymentPortal
     */
    private $sppClientPortal;

    /**
     * CaptureAction constructor.
     */
    public function __construct()
    {
        $this->sppClient = new SwedbankSppClient();
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $order = $payment->getOrder();

        $gatewayConfig = $payment->getMethod()->getGatewayConfig()->getConfig();

        $authentication = $this->sppClient->getAuthentication($gatewayConfig);

        $hpsTxn = $this->initializeHps($request, $order);
        $riskAction = $this->initializePaymentRiskData($order);
        $txnDetails = $this->initializePaymentDetail($riskAction, $order);

        /** @var Transaction $transaction */
        $transaction = new Transaction(
            $txnDetails,
            $hpsTxn,
            new Transaction\CardTxn()
        );

        /** @var SetupTransactionRequest $setupRequest */
        $setupRequest = new SetupTransactionRequest(
            $authentication,
            $transaction
        );

        /** @var SetupResponse $response */
        $response = $this->communication->sendData(
            $setupRequest,
            SetupResponse::class,
            $this->sppClient->getEndpointUrl($gatewayConfig['test'])
        );

        if ($response->getStatus() === 1) {
            $url = $response->getHpsTxn()->getHpsUrl()
                . '?HPS_SessionID=' .
                $response->getHpsTxn()->getSessionId();

            throw new HttpRedirect($url);
        }
    }

    /**
     * @param mixed $request
     *
     * @return bool
     */
    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }

    /**
     * @param                $request
     * @param OrderInterface $order
     *
     * @return HpsTxn
     */
    private function initializeHps($request, OrderInterface $order): HpsTxn
    {
        return new HpsTxn(
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_EXPIRY]),
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_PROCESSING]),
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_FAILED]),
            164,
            $this->getDynamicData($order),
            1,
            $this->getCustomerCardData($order->getCustomer(), $this->getPaymentMethod($order))
        );
    }

    /**
     * @param Transaction\Action $riskAction
     * @param OrderInterface $order
     * @return TxnDetails
     * @throws \Exception
     */
    private function initializePaymentDetail($riskAction, OrderInterface $order)
    {
        return new TxnDetails(
            $riskAction,
            $order->getNumber(),
            new Amount($order->getTotal() / 100 ),
            $this->appendThreeDSecurity($order)
        );
    }

    /**
     * @param OrderInterface $order
     *
     * @return ThreeDSecure|null
     * @throws \Exception
     */
    private function appendThreeDSecurity(OrderInterface $order): ?ThreeDSecure
    {
        $customer = $order->getCustomer();
        $gatewayConfig = $this->getPaymentMethod($order)->getGatewayConfig();

        $threedSecurity = new ThreeDSecure(
            $order->getNumber(),
            $this->getChannelHost($order->getChannel()),
            new \DateTime()
        );

        if ($this->getCreditCardToken($customer, $gatewayConfig) === null || $order->getTotal() > 3000) {
            return $threedSecurity;
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     * @return Action
     */
    private function initializePaymentRiskData(OrderInterface $order): Action
    {
        $riskAction = new Action(
            new MerchantConfiguration(
                'W',
                $order->getChannel()->getCountries()->first()->getName()
            ),
            new CustomerDetails(
                new BillingDetails(
                    'Mr',
                    $order->getBillingAddress()->getFullName(),
                    $order->getBillingAddress()->getPostcode(),
                    $order->getBillingAddress()->getStreet(),
                    '',
                    $order->getBillingAddress()->getCity(),
                    $order->getBillingAddress()->getCountryCode()
                ),
                new PersonalDetails(
                    $order->getBillingAddress()->getFirstName(),
                    $order->getBillingAddress()->getLastName(),
                    $order->getShippingAddress()->getPhoneNumber()
                ),
                new ShippingDetails(
                    'Mr', // title
                    $order->getShippingAddress()->getFirstName(),
                    $order->getShippingAddress()->getLastName(),
                    $order->getShippingAddress()->getStreet(),
                    '',
                    $order->getShippingAddress()->getCity(),
                    $order->getShippingAddress()->getCountryCode(),
                    $order->getShippingAddress()->getPostcode()
                ),
                new RiskDetails(
                    $order->getCustomerIp(),
                    $order->getCustomer()->getEmail()
                )
            )
        );

        return $riskAction;
    }

    /**
     * @param ChannelInterface $channel
     *
     * @return string
     */
    private function getChannelHost(ChannelInterface $channel): string
    {
        return 'https://' . $channel->getHostname();
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return Transaction\Card|null
     */
    private function getCustomerCardData(CustomerInterface $customer, PaymentMethodInterface $paymentMethod): ? Transaction\Card
    {
        $paymentConfig = $paymentMethod->getGatewayConfig();
        $creditCardToken = $this->getCreditCardToken($customer, $paymentConfig);

        if (null !== $creditCardToken) {
            return new Transaction\Card($creditCardToken);
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     *
     * @return Transaction\DynamicData
     */
    private function getDynamicData(OrderInterface $order): Transaction\DynamicData
    {
        $gatewayConfig = $this->getPaymentMethod($order)->getGatewayConfig();
        $customer = $order->getCustomer();

        if (null !== $this->getCreditCardToken($customer, $gatewayConfig)) {
            return new Transaction\DynamicData(
                null,
                $this->getChannelHost($order->getChannel()),
                $order->getChannel()->getName(),
                null,
                $customer->getPan(),
                $customer->getCardExpMonth(),
                $customer->getCardExpYear()
            );
        }

        return new Transaction\DynamicData(
            null,
            $this->getChannelHost($order->getChannel())
        );
    }

    /**
     * @param OrderInterface $order
     * @return PaymentMethodInterface
     * @throws \Exception
     */
    private function getPaymentMethod(OrderInterface $order): PaymentMethodInterface
    {
        /** @var PaymentInterface|false $payment */
        $payment = $order->getPayments()->first();

        if (false === $payment) {
            throw new \Exception(sprintf('Order: \'%s\' has no payments', $order->getId()));
        }

        $paymentMethod = $payment->getMethod();
        $paymentMethodGatewayName = $paymentMethod->getGatewayConfig()->getGatewayName();

        if ($paymentMethodGatewayName !== 'swedbank_spp') {
            throw new \Exception(
                sprintf(
                    'Invalid order\'s payment: \'%s\' method gateway. Expected: \'%s\' found: \'%s\'',
                    $payment->getId(),
                    'swedbank_spp',
                    $paymentMethodGatewayName
                )
            );
        }

        return $paymentMethod;
    }

    /**
     * @param CustomerInterface $customer
     * @param GatewayConfigInterface $paymentGatewayConfig
     * @return string|null
     */
    private function getCreditCardToken(
        CustomerInterface $customer,
        GatewayConfigInterface $paymentGatewayConfig
    ):? string {
        if (false === $this->useCreditCardTokenToken($paymentGatewayConfig)) {
            return null;
        }

        return $customer->getCreditCardToken();
    }

    /**
     * @param GatewayConfigInterface $paymentGatewayConfig
     * @return bool
     */
    private function useCreditCardTokenToken(GatewayConfigInterface $paymentGatewayConfig): bool
    {
        return true === $paymentGatewayConfig->getConfig()['use_token'];
    }
}
