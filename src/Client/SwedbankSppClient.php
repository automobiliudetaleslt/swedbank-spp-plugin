<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Client;

use GuzzleHttp\Client;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\Authentication as LocalAuthentication;
use SwedbankPaymentPortal\Options\CommunicationOptions;
use SwedbankPaymentPortal\Options\ServiceOptions;
use SwedbankPaymentPortal\SharedEntity\Authentication;
use SwedbankPaymentPortal\SwedbankPaymentPortal;

class SwedbankSppClient
{
    /**
     * @var Authentication
     */
    private $authentication;

    /**
     * @var Client
     */
    private $guzzleClient;

    /**
     * SwedbankSppClient constructor.
     */
    public function __construct()
    {
        $this->guzzleClient = new Client();
    }

    /**
     * @param array $gatewayConfig
     *
     * @return SwedbankPaymentPortal
     */
    public function initializePortal(array $gatewayConfig): SwedbankPaymentPortal
    {
        $this->authentication = new Authentication(
            $gatewayConfig['login'],
            $gatewayConfig['password']
        );

        $options = new ServiceOptions(
            new CommunicationOptions(
                $this->getEndpointUrl($gatewayConfig['test'])
            ),
            $this->authentication
        );

        SwedbankPaymentPortal::init($options);

        return SwedbankPaymentPortal::getInstance();
    }

    /**
     * @param array $gatewayConfig
     *
     * @return LocalAuthentication
     */
    public function getAuthentication(array $gatewayConfig): LocalAuthentication
    {
        return new LocalAuthentication(
            $gatewayConfig['login'],
            $gatewayConfig['password']
        );
    }

    /**
     * @param bool $testEnv
     * @return string
     */
    public function getEndpointUrl(bool $testEnv)
    {
        if ($testEnv === true) {
            return 'https://accreditation.datacash.com/Transaction/acq_a';
        }

        return 'https://mars.transaction.datacash.com/Transaction';
    }
}
