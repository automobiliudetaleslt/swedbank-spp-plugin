<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;
use JMS\Serializer\XmlSerializationVisitor;

/**
 *
 * @Annotation\AccessType("public_method")
 */
class Card
{
    const TYPE = 'hps_token';

    /**
     * Card expiry date.
     *
     * @var string
     *
     * @Annotation\Type("string")
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\SerializedName("expirydate")
     */
    private $expiryDate;

    /**
     * The masked card number captured from the hosted page .
     *
     * @var Pan
     *
     * @Annotation\SerializedName("pan")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\Pan")
     */
    private $pan;

    /**
     * Card constructor.
     * @param Pan $pan
     * @param string $expiryDate
     */
    public function __construct($pan, $expiryDate = null)
    {
        $this->pan = new Pan($pan);
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return string|null
     */
    public function getExpiryDate(): ?string
    {
        return $this->expiryDate;
    }

    /**
     * @param string $expiryDate
     */
    public function setExpiryDate(string $expiryDate): void
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return Pan
     */
    public function getPan(): ?Pan
    {
        return $this->pan;
    }

    /**
     * @param Pan $pan
     */
    public function setPan(Pan $pan): void
    {
        $this->pan = $pan;
    }
}
