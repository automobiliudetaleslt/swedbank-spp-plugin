<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;
use JMS\Serializer\XmlDeserializationVisitor;
use JMS\Serializer\XmlSerializationVisitor;
use JMS\Serializer\Context;

/**
 * The amount debited from the consumer’s account at Swedbank.
 *
 * The field type is a numeric value in cents.
 */
class Amount
{
    /**
     * The transaction amount.
     *
     * @var float
     *
     * @Annotation\Type("float")
     */
    private $value;

    /**
     * The transaction currency.
     *
     * @var string
     *
     * @Annotation\XmlAttribute
     * @Annotation\SerializedName("currency")
     * @Annotation\Type("string")
     * @Annotation\AccessType("reflection")
     */
    private $currency = 'EUR';

    /**
     * Amount constructor.
     *
     * @param float $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Value getter.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Value setter.
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Custom deserialization logic.
     *
     * @Annotation\HandlerCallback("xml", direction = "deserialization")
     *
     * @param XmlDeserializationVisitor $visitor
     * @param \SimpleXMLElement         $data
     * @param Context                   $context
     */
    public function deserialize(XmlDeserializationVisitor $visitor, $data, Context $context)
    {
        $this->currency = (string)$this->getAttribute($data->attributes(), 'currency');
        $this->value = (string)$data;
    }

    /**
     * Custom serialization logic.
     *
     * @Annotation\HandlerCallback("xml", direction = "serialization")
     *
     * @param XmlSerializationVisitor $visitor
     */
    public function serialize(XmlSerializationVisitor $visitor)
    {
        /** @var \DOMElement $node */
        $node = $visitor->getCurrentNode();
        $node->setAttribute('currency', $this->currency);
        $node->nodeValue = $this->value;
    }

    /**
     * Returns attribute.
     *
     * @param \SimpleXMLElement $attributes
     * @param string            $key
     *
     * @throws \InvalidArgumentException
     *
     * @return mixed
     */
    private function getAttribute($attributes, $key)
    {
        if (!isset($attributes[$key])) {
            throw new \InvalidArgumentException("Attribute {$key} not set.");
        }

        return $attributes[$key];
    }
}
