<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use Jms\Serializer\Annotation;

/**
 * The container that contains the data to be risk screened.
 *
 * @Annotation\AccessType("public_method")
 */
class Risk
{
    /**
     * This will indicate when if the transaction is to be screened pre or post authorisation.
     *
     * @var Action
     *
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\Action")
     * @Annotation\SerializedName("Action")
     */
    private $action;

    /**
     * Risk constructor.
     *
     * @param Action $action
     */
    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    /**
     * Action getter.
     *
     * @return Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Action setter.
     *
     * @param Action $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
}
