<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use Jms\Serializer\Annotation;

/**
 * The container for the payment type details.
 *
 * @Annotation\AccessType("reflection")
 */
class PaymentDetails
{
    /**
     * This is the mechanism with which the client chooses to purchase. Should be populated with CC = Bank Card.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("payment_method")
     */
    private $paymentMethod = 'CC';
}
