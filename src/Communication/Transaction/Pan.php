<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;
use JMS\Serializer\XmlSerializationVisitor;

/**
 *
 * @Annotation\AccessType("public_method")
 */
class Pan
{
    /**
     * @Annotation\XmlAttribute()
     */
    private $type = 'hps_token';

    /**
     * The masked card number captured from the hosted page .
     *
     * @var string
     *
     * @Annotation\Type("string")
     * @Annotation\Inline()
     * @Annotation\XmlElement(cdata=false)
     */
    private $pan;

    /**
     * Card constructor.
     *
     * @param string $pan
     * @param null $expiryDate
     */
    public function __construct($pan)
    {
        $this->pan = $pan;
    }

    /**
     * @return string
     */
    public function getPan(): string
    {
        return $this->pan;
    }

    /**
     * @param string $pan
     */
    public function setPan(string $pan): void
    {
        $this->pan = $pan;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
