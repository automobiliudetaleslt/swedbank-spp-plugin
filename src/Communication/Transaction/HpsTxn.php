<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation;

/**
 * The container for the details pertaining to the card transaction.
 *
 * @Annotation\AccessType("public_method")
 */
class HpsTxn
{
    /**
     * The Page Set ID which corresponds to the Hosted Page configuration onthe Gateway.
     *
     * @var int
     *
     * @Annotation\Type("integer")
     * @Annotation\SerializedName("page_set_id")
     */
    private $pageSetId = 1;

    /**
     * Indicates the Hosted Method to be used. Value must match = “setup_full”.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     */
    private $method = 'setup_full';

    /**
     * A URL to which the customer will be returned when the transaction processing has been completed.
     *
     * A secure (https) URL must be provided. The customer will be returned
     * to this URL only after a successful transaction.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("return_url")
     */
    private $returnUrl;

    /**
     * A URL to which the customer will be returned if the session ID has expired.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("expiry_url")
     */
    private $expiryUrl;

    /**
     * A URL to which the customer will be returned to on error.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("error_url")
     */
    private $errorUrl;

    /**
     * The container for the Dynamic Data that is used to display information on the hosted page.
     *
     * @var DynamicData
     *
     * @Annotation\SerializedName("DynamicData")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\DynamicData")
     */
    private $dynamicData;

    /**
     * A URL to which the customer will be returned to on error.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("integer")
     * @Annotation\SerializedName("cvv_only")
     */
    private $cvvOnly;

    /**
     * The container for the Dynamic Data that is used to display information on the hosted page.
     *
     * @var Card
     *
     * @Annotation\SerializedName("Card")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\Card")
     */
    private $card;

    /**
     * HpsTxn constructor.
     * @param string $expiryUrl
     * @param string $returnUrl
     * @param string $errorUrl
     * @param int $pageSetId
     * @param DynamicData|null $dynamicData
     * @param int $cvvOnly
     * @param Card|null $card
     */
    public function __construct(
        $expiryUrl,
        $returnUrl,
        $errorUrl,
        $pageSetId = 1,
        DynamicData $dynamicData = null,
        $cvvOnly = 0,
        Card $card = null
    ) {
        $this->errorUrl = $errorUrl;
        $this->expiryUrl = $expiryUrl;
        $this->returnUrl = $returnUrl;
        $this->pageSetId = $pageSetId;
        $this->dynamicData = $dynamicData;
        $this->cvvOnly = $cvvOnly;
        $this->card = $card;
    }

    /**
     * @return int
     */
    public function getPageSetId(): int
    {
        return $this->pageSetId;
    }

    /**
     * @param int $pageSetId
     */
    public function setPageSetId(int $pageSetId): void
    {
        $this->pageSetId = $pageSetId;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getReturnUrl(): string
    {
        return $this->returnUrl;
    }

    /**
     * @param string $returnUrl
     */
    public function setReturnUrl(string $returnUrl): void
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return string
     */
    public function getExpiryUrl(): string
    {
        return $this->expiryUrl;
    }

    /**
     * @param string $expiryUrl
     */
    public function setExpiryUrl(string $expiryUrl): void
    {
        $this->expiryUrl = $expiryUrl;
    }

    /**
     * @return string
     */
    public function getErrorUrl(): string
    {
        return $this->errorUrl;
    }

    /**
     * @param string $errorUrl
     */
    public function setErrorUrl(string $errorUrl): void
    {
        $this->errorUrl = $errorUrl;
    }

    /**
     * @return DynamicData
     */
    public function getDynamicData(): DynamicData
    {
        return $this->dynamicData;
    }

    /**
     * @param DynamicData $dynamicData
     */
    public function setDynamicData(DynamicData $dynamicData): void
    {
        $this->dynamicData = $dynamicData;
    }

    /**
     * @return string
     */
    public function getCvvOnly(): int
    {
        return $this->cvvOnly;
    }

    /**
     * @param int $cvvOnly
     */
    public function setCvvOnly(int $cvvOnly): void
    {
        $this->cvvOnly = $cvvOnly;
    }

    /**
     * @return Card|null
     */
    public function getCard(): ?Card
    {
        return $this->card;
    }

    /**
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}
