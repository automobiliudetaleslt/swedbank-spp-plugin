<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use Jms\Serializer\Annotation;

/**
 * The container for the transaction.
 *
 * @Annotation\AccessType("public_method")
 */
class OrderDetails
{
    /**
     * The container transaction details.
     *
     * @var BillingDetails
     *
     * @Annotation\SerializedName("BillingDetails")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\BillingDetails")
     */
    private $billingDetails;

    /**
     * OrderDetails constructor.
     *
     * @param BillingDetails $billingDetails
     */
    public function __construct(BillingDetails $billingDetails)
    {
        $this->billingDetails = $billingDetails;
    }

    /**
     * BillingDetails getter.
     *
     * @return BillingDetails
     */
    public function getBillingDetails()
    {
        return $this->billingDetails;
    }

    /**
     * BillingDetails setter.
     *
     * @param BillingDetails $billingDetails
     */
    public function setBillingDetails($billingDetails)
    {
        $this->billingDetails = $billingDetails;
    }
}
