<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication;

use Jms\Serializer\Annotation;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\CardTxn;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\HpsTxn;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\TxnDetails;

class Transaction
{
    /**
     * The container transaction details.
     *
     * @var TxnDetails
     *
     * @Annotation\SerializedName("TxnDetails")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\TxnDetails")
     */
    private $txnDetails;

    /**
     * The container for the HPS (hosted page) details.
     *
     * @var HpsTxn
     *
     * @Annotation\SerializedName("HpsTxn")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\HPSTxn")
     */
    private $hpsTxn;

    /**
     * The container for the HPS (hosted page) details.
     *
     * @var HpsTxn
     *
     * @Annotation\SerializedName("CardTxn")
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\CardTxn")
     */
    private $cardTxn;

    /**
     * Transaction constructor.
     * @param TxnDetails $txnDetails
     * @param HpsTxn $hpsTxn
     * @param CardTxn $cardTxn
     */
    public function __construct(TxnDetails $txnDetails, HPSTxn $hpsTxn, CardTxn $cardTxn)
    {
        $this->txnDetails = $txnDetails;
        $this->hpsTxn = $hpsTxn;
        $this->cardTxn = $cardTxn;
    }

    /**
     * TxnDetails getter.
     *
     * @return TxnDetails
     */
    public function getTxnDetails()
    {
        return $this->txnDetails;
    }

    /**
     * TxnDetails setter.
     *
     * @param TxnDetails $txnDetails
     */
    public function setTxnDetails($txnDetails)
    {
        $this->txnDetails = $txnDetails;
    }

    /**
     * HpsTxn getter.
     *
     * @return HPSTxn
     */
    public function getHpsTxn()
    {
        return $this->hpsTxn;
    }

    /**
     * HpsTxn setter.
     *
     * @param HPSTxn $hpsTxn
     */
    public function setHpsTxn($hpsTxn)
    {
        $this->hpsTxn = $hpsTxn;
    }

    /**
     * @return HpsTxn
     */
    public function getCardTxn(): HpsTxn
    {
        return $this->cardTxn;
    }

    /**
     * @param HpsTxn $cardTxn
     */
    public function setCardTxn(HpsTxn $cardTxn): void
    {
        $this->cardTxn = $cardTxn;
    }
}